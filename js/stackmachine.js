// constantes dos comandos validos
MAIN_COMMANDS = "push,pop,load,store,add,sub,label,goto,gotof,eq,ne,gt,lt,ge,le,print,read,end";
CMDS_PARAM_STR = "load,store,label,goto,gotof";
CMDS_PARAM_CONST = "push,read";
CMDS_NO_PARAM = "pop,add,sub,eq,ne,gt,lt,ge,le,print,end";


// verifica se o comando é válido
function validate(cmd) {
  cmd = cmd.toLowerCase()
  var flag = false;
  if (isNaN(cmd)) {
    if (cmd.indexOf("(") > 0 && cmd.indexOf(")") > 0) {
      MAIN_COMMANDS.split(",").forEach(function(i) {
        var fcmd = cmd.substr(0, cmd.indexOf("("));
        if (fcmd === i) {
          // valida o parâmetro
          var param = cmd.substring(cmd.indexOf("(") + 1, cmd.indexOf(")"));
          if (param == "" && CMDS_NO_PARAM.search(fcmd) >= 0) flag = true;
          else if (param != "" && CMDS_PARAM_CONST.search(fcmd) >= 0) {
            if (!isNaN(param) || param === "true" || param === "false")
              flag = true;
            else flag = false;
          }
          else if (isNaN(param) && param != "" && CMDS_PARAM_STR.search(fcmd) >= 0)
            flag = true;
          else flag = false;
        }
      });
    }
    return flag;
  }
}


// operações da máquina de pilha
function StackMachine($scope) {

  $scope.incmd = [];
  $scope.bytecodes = [];
  $scope.stack = [];
  $scope.record = [];
  var pointer = 0;

  $scope.incmdPush = function() {
    if (validate($scope.pushText)) {
      $scope.incmd.push(
      {
      	command: $scope.pushText.substr(0, $scope.pushText.indexOf("(")),
      	param:$scope.pushText.substring($scope.pushText.indexOf("(") + 1,
      	$scope.pushText.indexOf(")")),
        pointer: false,
      });
    } else alert("Comando inválido!");
    $scope.pushText = '';
  };

  $scope.incmdClean = function() {
    $scope.incmd = [];
  };

  $scope.incmdLoad = function() {
		if ($scope.incmd.length > 0)
			$scope.bytecodes.push($scope.incmd.shift());
		else alert("A entrada está vazia");
	};

	$scope.bytecodesReset = function() {
    if (pointer > 0) {
      pointer--;
      $scope.bytecodes[pointer].pointer = false;
      $scope.bytecodes[pointer - 1].pointer = true;
    }
    else if (pointer < 0) pointer = 0;
	};

	$scope.bytecodesNext = function() {
    if (pointer < $scope.bytecodes.length) {
      $scope.bytecodes[pointer].pointer = true;
      eval("stack" + $scope.bytecodes[pointer].command + "()");
    }
	};

  stackpush = function() {
    $scope.stack.unshift($scope.bytecodes[pointer].param);
    if (pointer > 0) $scope.bytecodes[pointer - 1].pointer = false;
    pointer++;
  };

  stackpop = function() {
    $scope.stack.shift();
    $scope.bytecodes[pointer - 1].pointer = false;
    pointer++;
  };

  stackload = function() {
    var name = $scope.bytecodes[pointer].param;
    var index = -1;
    for (var i = 0; i < $scope.record.length; i++) {
      if ($scope.record[i].varname == name) index = i;
    }
    if (index > -1) {
      $scope.stack.unshift($scope.record[index].varvalue);
      $scope.bytecodes[pointer - 1].pointer = false;
      pointer++;
    } else alert("variável não existe!");
  };

  stackstore = function() {
    var name = $scope.bytecodes[pointer].param;
    var index = -1;
    for (var i = 0; i < $scope.record.length; i++) {
      if ($scope.record[i].varname == name) index = i;
    }
    if (index == -1) {
      var val = $scope.stack.shift();
      $scope.record.push({varname: name, varvalue: val});
    } else {
      var val = $scope.stack.shift();
      $scope.record[index] = ({varname: name, varvalue: val});
    }
    $scope.bytecodes[pointer - 1].pointer = false;
    pointer++;
  };

  stackadd = function() {
    var p1 = $scope.stack[0];
    var p2 = $scope.stack[1];
    if (!isNaN(p1) && !isNaN(p2) && p1 !== "" && p2 !== "") {
      p1 = $scope.stack.shift();
      p2 = $scope.stack.shift();
      $scope.stack.unshift(parseInt(p1) + parseInt(p2));
      $scope.bytecodes[pointer - 1].pointer = false;
      pointer++;
    } else alert("Impossível realizar operação!");
  };

  stacksub = function() {
    var p1 = $scope.stack[0];
    var p2 = $scope.stack[1];
    if (!isNaN(p1) && !isNaN(p2) && p1 !== "" && p2 !== "") {
      p1 = $scope.stack.shift();
      p2 = $scope.stack.shift();
      $scope.stack.unshift(parseInt(p1) - parseInt(p2));
      $scope.bytecodes[pointer - 1].pointer = false;
      pointer++;
    } else alert("Impossível realizar operação!");
  };

  stacklabel = function() {
    $scope.bytecodes[pointer - 1].pointer = false;
    pointer++;
  };

  stackgoto = function() {
    if ($scope.stack[0].toLowerCase() == "true") {
      var name = $scope.bytecodes[pointer].param;
      var index = -1;
      for (var i = 0; i < $scope.bytecodes.length; i++) {
        if ($scope.bytecodes[i].param == name &&
          $scope.bytecodes[i].command == "label") index = i;
      }
      if (index > -1) {
        pointer = index;
        $scope.stack.shift();
      } else {
        alert("Não foi possível desviar fluxo");
        $scope.bytecodes[pointer - 1].pointer = false;
        pointer++;
      }
    } else if ($scope.stack[0].toLowerCase() == "false") {
      $scope.stack.shift();
      $scope.bytecodes[pointer - 1].pointer = false;
      pointer++;
    } else pointer++;
  };

  stackgotof = function() {
    if ($scope.stack[0].toLowerCase() == "false") {
      var name = $scope.bytecodes[pointer].param;
      var index = -1;
      for (var i = 0; i < $scope.bytecodes.length; i++) {
        if ($scope.bytecodes[i].param == name &&
          $scope.bytecodes[i].command == "label") index = i;
      }
      if (index > -1) {
        pointer = index;
        $scope.stack.shift();
      } else {
        alert("Não foi possível desviar fluxo");
        $scope.bytecodes[pointer - 1].pointer = false;
        pointer++;
      }
    } else if ($scope.stack[0].toLowerCase() == "true") {
      $scope.stack.shift();
      $scope.bytecodes[pointer - 1].pointer = false;
      pointer++;
    } else pointer++;
  };

  stackeq = function() {
    var val1 = $scope.stack.shift()
    var val2 = $scope.stack.shift()
    if (val1 == val2) $scope.stack.unshift("true")
    else $scope.stack.unshift("false")
    $scope.bytecodes[pointer - 1].pointer = false;
    pointer++;
  };

  stackne = function() {
    var val1 = $scope.stack.shift()
    var val2 = $scope.stack.shift()
    if (val1 != val2) $scope.stack.unshift("true")
    else $scope.stack.unshift("false")
    $scope.bytecodes[pointer - 1].pointer = false;
    pointer++;
  };

  stackgt = function() {
    var val1 = $scope.stack.shift()
    var val2 = $scope.stack.shift()
    if (val1 > val2) $scope.stack.unshift("true")
    else $scope.stack.unshift("false")
    $scope.bytecodes[pointer - 1].pointer = false;
    pointer++;
  };

  stacklt = function() {
    var val1 = $scope.stack.shift()
    var val2 = $scope.stack.shift()
    if (val1 < val2) $scope.stack.unshift("true")
    else $scope.stack.unshift("false")
    $scope.bytecodes[pointer - 1].pointer = false;
    pointer++;
  };

  stackge = function() {
    var val1 = $scope.stack.shift()
    var val2 = $scope.stack.shift()
    if (val1 >= val2) $scope.stack.unshift("true")
    else $scope.stack.unshift("false")
    $scope.bytecodes[pointer - 1].pointer = false;
    pointer++;
  };

  stackle = function() {
    var val1 = $scope.stack.shift()
    var val2 = $scope.stack.shift()
    if (val1 <= val2) $scope.stack.unshift("true")
    else $scope.stack.unshift("false")
    $scope.bytecodes[pointer - 1].pointer = false;
    pointer++;
  };

  stackprint = function() {
    alert($scope.stack[0]);
    $scope.bytecodes[pointer - 1].pointer = false;
    pointer++;
  };

  stackread = function() {
    $scope.stack.unshift($scope.bytecodes[pointer].param);
    $scope.bytecodes[pointer - 1].pointer = false;
    pointer++;
  };

  stackend = function() {
    pointer = 0;
    $scope.stack = []
    $scope.incmd = []
    $scope.record = []
    alert("Fim da execução");
  };
}
